package test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);

	private static void acceptValue(double[] value) {
		System.out.print("Enter coefficient : ");
		value[0] = sc.nextDouble();
	}
	private static void acceptValue1(double[] value) {
		System.out.print("Input the value of variable a  : ");
		value[0] = sc.nextDouble();
	}
	
	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Display All Elements");
		System.out.println("2. Add Node at First");
		System.out.println("3. Add Node at Last");
		System.out.println("4. Display Polinomial Function");
		System.out.println("5. Input the value of variable a");
		System.out.println("Enter Choice : ");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		int choice;
		SinglyLinearLinkedList list = new SinglyLinearLinkedList();
		double[] value = new double[1];
		while((choice = Program.menu()) != 0) {
			switch(choice) {
			case 1 ://Display All
				try {
					list.displayAll();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 2 ://Add First
				Program.acceptValue(value);
				list.addCoeffFromFirst(value[0]);
				break;
			case 3 ://Add Last
				Program.acceptValue(value);
				list.addCoeffFromLast(value[0]);
				break;
			case 4 ://Add Last
				list.dispalPolynomialFun();;
				break;
			case 5 ://Add Last
				Program.acceptValue1(value);
				double res = list.polynomialFun(value[0]);
				System.out.println("Result : "+res);
				break;
			}
			System.out.println();
		}
	}

}
