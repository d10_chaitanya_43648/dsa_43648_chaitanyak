package test;

public class SinglyLinearLinkedList {
	// Node is static member class of SinglyLinearLinkedList
	static class Node{
		// Node class fields
		private double data;
		private Node next;
		// Node class methods
		public Node() {
			data = 0.0;
			next = null;
		}
		public Node(double value) {
			data = value;
			next = null;
		}
	}
	
	// SinglyLinearLinkedList class fields
	private Node head;
	
	// SinglyLinearLinkedList class methods
	public SinglyLinearLinkedList() {
		head = null;
	}
	
	//Check list is Empty or not
	public boolean isEmpty() {
		return head == null;
	}

	public void displayAll() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("List : ");
			Node trav = head;
			while(trav != null) {
				System.out.println(trav.data);
				trav = trav.next;
			}
			System.out.println();
		}
	}

	public void addCoeffFromFirst(double value) {
		// create new node and init it
		Node newNode = new Node(value);
		if(isEmpty())
			head = newNode;
		else {
			// new node next should point to head
			newNode.next = head;
			// head should point to new node
			head = newNode;
		}
	}

	public void addCoeffFromLast(double value) {
		// create new node and init it
		Node newNode = new Node(value);
		// special 1: if list is empty, make new node as first node of list
		if(isEmpty())
			head = newNode;
		// general: add node at the end
		else {
			Node trav = head;
			// traverse till last node
			while(trav.next != null)
				trav = trav.next;
			// add new node after trav (trav.next)
			trav.next = newNode;
		}	
	}
	public void dispalPolynomialFun() {
		//f(a) = -2a$3 + 5a - 3
		// f(a) = -2a$3 + 0a$2 + 5a$1 + -3a$0
		Node trav = head;
		int power = 0; 
		String str  = "";
		while(trav != null) {
			str = trav.data + "a$" + power + " + " + str;
			power++;
			trav = trav.next;
		}
		System.out.println("f(a) = "+str);
	}
	
	public double polynomialFun(double val) {
		//f(a) = -2a$3 + 5a - 3
		// f(a) = -2a$3 + 0a$2 + 5a$1 + -3a$0
		Node trav = head;
		int power = 0; 
		double total = 0;
		while(trav != null) {
			total = total + Math.multiplyExact((long)trav.data, (long)(Math.pow(val, power))); 
			power++;
			trav = trav.next;
		}
		return total;
	}
		
}
