package binary;

import java.util.Arrays;

public class Program {
	public static int selectionSort(double[] arr) {
		int swaps=0;
		for (int i = 0; i < arr.length-1; i++) {
			for (int j = i+1; j < arr.length; j++) {
				if(arr[i]> arr[j])
				{
					double temp = arr[i];
					arr[i]=arr[j];
					arr[j]=temp;
					swaps++;
				}
				
			}
			
		}
		return swaps;
	
	}
	
	public static void main(String[] args) {
		double[] arr = {20.4,9.5,5.4,2.2,2.1,2};
		
		
		 int swap = Program.selectionSort(arr);
		System.out.println(" sorted array swaps are  "+swap);
		
	}
	}


