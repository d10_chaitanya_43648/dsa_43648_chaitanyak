package test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	
	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Infix Expression : ");
		System.out.println("2. Infix to Postfix");
		System.out.println("3. Infix to Prefix");
		System.out.println("4. Postfix Evaluation");
		System.out.println("5. Prefix Evaluation");
		System.out.print("Enter Choice : ");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		int choice;
		String[] str = new String[1];
		String postfix = "", prefix = "";
		int result;
		while((choice = Program.menu()) != 0) {
			switch(choice) {
			case 1:
				Program.acceptInfixExpression(str);//12 + 48 / ( 32 - 28 ) * -4 + 2 $ 5
				break;
			case 2:
				postfix = Expression.infixToPostfix(str[0]);
				System.out.println("Postfix : "+postfix);
				break;
			case 3:
				prefix = Expression.infixToPrefix(str[0]);
				System.out.println("Prefix : "+prefix);
				break;
			case 4:
				result = Expression.solvePostfix(postfix);
				System.out.println("Result: " + result);
				break;
			case 5:
				result = Expression.solvePostfix(postfix);
				System.out.println("Result: " + result);
				break;
			}
		}
	}

	private static void acceptInfixExpression(String[] str) {
		sc.nextLine();
		System.out.print("Enter Infix Expression  : ");
		str[0] = sc.nextLine();
	}

}
