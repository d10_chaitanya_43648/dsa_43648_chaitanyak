package test;

import java.util.Stack;

public class Expression {

	public static int pri(String opr) {
		char operator = opr.charAt(0);
		switch (operator) {
		case '$': return 3;
		case '*': return 2;
		case '/': return 2;
		case '%': return 2;
		case '+': return 1;
		case '-': return 1;
		}
		return 0;
	}
	
	public static boolean isNumber(String s) {
		int num;
		try {
			num = Integer.parseInt(s);
			assert -999 < num :"Not valid";
			assert num < 999 :"Not valid";
			if(num >= -999 && num <= 999)
				return true;
		} catch (NumberFormatException e) {
		}
		
		return false;
	}
	
	public static String infixToPostfix(String infix) {
		Stack<String> s = new Stack<String>();
		StringBuilder post = new StringBuilder();
		String[] str = infix.split(" ");
		//1. traverse infix expression from left to right
		for(int i = 0; i < str.length; i++) {
			String sym = str[i]; // sym can be operand, (, ) or operator
			//2. if operand is found, append to postfix.
			if(isNumber(sym)) {
				post.append(sym);
				post.append(" ");
			}
			else if(sym.equalsIgnoreCase("(")) //4. if opening ( is found, then push it on stack.
				s.push(sym);
			else if(sym.equalsIgnoreCase(")")) { //5. if closing ) is found, 
				// pop all operators from stack one by one and append to postfix
				// until opening ( is found.
				while(!(s.peek().equalsIgnoreCase("("))) {
					post.append(s.pop());
					post.append(" ");
				}
				// also pop and discard opening (
				s.pop();
			} 
			else { //3. if "operator" is found, push it on the stack.
				// if priority of topmost operator from stack >= priority of current operator,
				while(!s.isEmpty() && pri(s.peek()) >= pri(sym)) {
					// pop operator from stack and append to postfix.
					post.append(s.pop());
					post.append(" ");
				}
				s.push(sym);
				
			}		
		} // end of for loop
		//6. when all syms from infix are done, pop all operators from stack one by one and append to postfi
		while(!s.isEmpty()) {
			post.append(s.pop());
			post.append(" ");
		}
		return post.toString();
	}
	
	public static String infixToPrefix(String infix) {
		Stack<String> s = new Stack<String>();
		StringBuilder pre = new StringBuilder();
		String[] str = infix.split(" ");
		//1. traverse infix expression from left to right
		for(int i = str.length - 1; i >= 0; i--) {
			String sym = str[i]; // sym can be operand, (, ) or operator
			//2. if operand is found, append to postfix.
			if(isNumber(sym)) {
				pre.append(sym);
				pre.append(" ");
			}
			else if(sym.equalsIgnoreCase(")")) //4. if opening ( is found, then push it on stack.
				s.push(sym);
			else if(sym.equalsIgnoreCase("(")) { //5. if closing ) is found, 
				// pop all operators from stack one by one and append to postfix
				// until opening ( is found.
				while(!(s.peek().equalsIgnoreCase(")"))) {
					pre.append(s.pop());
					pre.append(" ");
				}
				// also pop and discard opening (
				s.pop();
			} 
			else { //3. if "operator" is found, push it on the stack.
				// if priority of topmost operator from stack >= priority of current operator,
				while(!s.isEmpty() && pri(s.peek()) > pri(sym)) {
					// pop operator from stack and append to postfix.
					pre.append(s.pop());
					pre.append(" ");
				}
				s.push(sym);
				
			}		
		} // end of for loop
		//6. when all syms from infix are done, pop all operators from stack one by one and append to postfi
		while(!s.isEmpty()) {
			pre.append(s.pop());
			pre.append(" ");
		}
		String[] rev = pre.toString().split(" ");
		String str1 = "";
		for (int i = rev.length - 1; i >= 0; i--) {
			str1 = str1 + rev[i] + " ";
		}
		return str1;
	}
	
	public static int cal(int a, int b, String sym) {
		char ch = sym.charAt(0);
		switch (ch) {
			case '$': return (int)Math.pow(a, b);
			case '*': return a * b;
			case '/': return a / b;
			case '%': return a % b;
			case '+': return a + b;
			case '-': return a - b;
		}
		return 0;
	}
	
	public static int solvePostfix(String post) {
		// stack of operands
		Stack<Integer> s = new Stack<Integer>();
		String[] str = post.split(" ");
		// traverse postfix from left to right
		for (int i = 0; i < str.length; i++) {
			// get each sym from expression
			String sym = str[i]; // sym can be operand, (, ) or operator
			//2. if operand is found, append to postfix.
			if(isNumber(sym))
				s.push(Integer.parseInt(sym));
			else {
				// pop two operands from stack
				int b = s.pop();
				int a = s.pop();
				// calculate the result
				int c = cal(a, b, sym);
				// push result on stack
				s.push(c);
			}
		}// repeat for all syms in expression
		// pop final result from stack and return.
		return s.pop();
	}
	
	public static int solvePrefix(String pre) {
		// stack of operands
		Stack<Integer> s = new Stack<Integer>();
		String[] str = pre.split(" ");
		// traverse postfix from left to right
		for (int i = str.length - 1; i >= 0; i--) {
			// get each sym from expression
			String sym = str[i]; // sym can be operand, (, ) or operator
			//2. if operand is found, append to postfix.
			if(isNumber(sym))
				s.push(Integer.parseInt(sym));
			else {
				// pop two operands from stack
				int a = s.pop();
				int b = s.pop();
				// calculate the result
				int c = cal(a, b, sym);
				// push result on stack
				s.push(c);
			}
		}// repeat for all syms in expression
		// pop final result from stack and return.
		return s.pop();
	}
}
