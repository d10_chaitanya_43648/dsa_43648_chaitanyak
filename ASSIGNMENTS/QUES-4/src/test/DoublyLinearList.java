package test;


public class DoublyLinearList {
	// Node is static member class of DoublyLinearLinkedList
	static class Node{
		// Node class fields
		private Object data;
		private Node prev;
		private Node next;
		// Node class methods
		public Node() {
			data = 0;
			prev = null;
			next = null;
		}
		public Node(Object value) {
			data = value;
			prev = null;
			next = null;
		}
	}
	
	// DoublyLinearLinkedList class fields
	private Node head;
	
	// DoublyLinearLinkedList class methods
	public DoublyLinearList() {
		head = null;
	}
	
	//Check list is Empty or not
	public boolean isEmpty() {
		return head == null;
	}

	public void displayForward() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("List : ");
			Node trav = head;
			while(trav != null) {
				System.out.println(trav.data.toString());
				trav = trav.next;
			}
			System.out.println();
		}
	}
	
	public void displayBackward() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("List : ");
			Node trav = head;
			// traverse till last node
			while(trav.next != null)
				trav = trav.next;
			// print all nodes in rev direction
			while(trav != null) {
				System.out.println(trav.data.toString());
				trav = trav.prev;
			}
			System.out.println();
		}
	}

	public void addFirst(Object value) {
		// create new node and init it
		Node newNode = new Node(value);
		// special 1: if list is empty
		if(isEmpty()) {
			head = newNode;
		}
		// general: add node at the start
		else {
			// new node next should point to head
			newNode.next = head;
			// head's prev to newNode
			head.prev = newNode;
			// head should point to new node
			head = newNode;
		}
	}

	public void addLast(Object value) {
		// create new node & init it
		Node newNode = new Node(value);
		// special 1: if list is empty, make new node as first node of list
		if(head == null) {
			head = newNode;
		}
		// general: add node at the end
		else {
			// traverse till last node
			Node trav = head;
			while(trav.next != null)
				trav = trav.next;
			// add new node after trav (trav.next)
			trav.next = newNode;
			// newnode's prev to last node (trav)
			newNode.prev = trav;
		}	
	}

	public void addAtPos(Object value, int pos) {
		// special 1: if list is empty, add node at the start
		// special 2: if pos<=1, add node at the start
		if(head == null || pos <= 1)
			addFirst(value);
		// general: add node at given pos
		else {
			// allocate & init new node
			Node newNode = new Node(value);
			// traverse till pos-1 (trav)
			Node temp = null, trav = head;
			for(int i=1; i < pos-1; i++) {
				// special 3: if pos > length of list, add node at the end.
				if(trav.next == null)
					break;
				trav = trav.next;
			}
			// take temp pointer to node after trav
			temp = trav.next;
			// newnode's next should point to trav's next i.e. temp
			newNode.next = temp;
			// newnode's prev to trav
			newNode.prev = trav;
			// trav's next should pointer to new node
			trav.next = newNode;
			// temp's prev to newNode
			if(temp != null) // special 3: if adding at end, next line is not required.
				temp.prev = newNode;
		}
	}

	public void delFirst() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is empty.");
		// special 2: if list has single node, make head null
		if(head.next == null)
			head = null;
		// general: 
		else {
			// make head pointing to next node.
			head = head.next;
			// note: the old first node will be garbage collected.
			// second node (new head) prev should be null
			head.prev = null;
		}		
	}
	
	public void delLast() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		// special 2: if list has single node, make head null.
		if(head.next == null)
			head = null;
		// general: delete the last node
		else {
			Node temp = null, trav = head;
			// traverse till last node (trav) and run temp behind it
			while(trav.next != null) {
				temp = trav;
				trav = trav.next;
			}
			// when last node (trav) is deleted, second last node (temp) next should be null.
			temp.next = null;
			//trav's prev contain the reference previous node so make it null
			trav.prev = null;
			// last node (trav) will be garbage collected.
		}
		
	}

	public void delAtPos(int pos) {
		// special 1: if pos = 1, delete first node
		if(pos == 1) {
			delFirst();
			return;
		}
		// special 2: if list is empty or pos < 1, then throw exception.
		if(isEmpty() || pos < 1)
			throw new RuntimeException("List is empty or Invalid position.");
		else {
			Node temp = null, trav = head;
			// traverse till pos (trav)
			for(int i = 1; i < pos; i++) {
				// special 3: if pos is beyond list length, then throw exception.
				if(trav.next == null)
					throw new RuntimeException("Invalid position.");
				temp = trav;
				trav = trav.next;
			}
			// temp next node to trav's next node
			temp.next = trav.next;
			// trav's next node's prev to trav's previous node
			if(trav.next != null) // special 3: while deleting last node, skip next line.
				trav.next.prev = trav.prev;
			// trav node will be garbage collected
		}
	}


	public void delAll() {
		head = null;
		// all nodes will be garbage collected
	}
		
}

