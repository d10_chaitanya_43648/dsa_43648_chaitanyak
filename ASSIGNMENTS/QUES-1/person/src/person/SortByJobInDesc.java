package test;

import java.util.Comparator;

public class SortByJobInDesc implements Comparator<Employee>{
	@Override
	public int compare(Employee o1, Employee o2) {
		return o2.getJob().compareTo(o1.getJob());
	}

}
