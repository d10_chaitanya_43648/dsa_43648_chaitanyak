package test;

import java.util.Comparator;

public class SortByDeptno implements Comparator<Employee>{
	@Override
	public int compare(Employee o1, Employee o2) {
		return o1.getDeptno() - o2.getDeptno();
	}

}
