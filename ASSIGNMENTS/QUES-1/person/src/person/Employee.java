package test;

public class Employee implements Comparable<Employee>{
	private  int empno;
	private String ename, job;
	private int mgr;
	private String hireDate;
	private double salary, comm;
	private int deptno;
	public Employee() {
	}
	public Employee(int empno, String ename, String job, int mgr, String hireDate, double salary, double comm,
			int deptno) {
		
		this.empno = empno;
		this.ename = ename;
		this.job = job;
		this.mgr = mgr;
		this.hireDate = hireDate;
		this.salary = salary;
		this.comm = comm;
		this.deptno = deptno;
	}
	public int getEmpno() {
		return empno;
	}
	public void setEmpno(int empno) {
		this.empno = empno;
	}
	public String getEname() {
		return ename;
	}
	public void setEname(String ename) {
		this.ename = ename;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public int getMgr() {
		return mgr;
	}
	public void setMgr(int mgr) {
		this.mgr = mgr;
	}
	public String getHireDate() {
		return hireDate;
	}
	public void setHireDate(String hireDate) {
		this.hireDate = hireDate;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}
	public double getComm() {
		return comm;
	}
	public void setComm(double comm) {
		this.comm = comm;
	}
	public int getDeptno() {
		return deptno;
	}
	public void setDeptno(int deptno) {
		this.deptno = deptno;
	}
	
	@Override
	public int compareTo(Employee o) {	
		return this.getEmpno() - o.getEmpno();
	}
	
	@Override
	public boolean equals(Object o) {
		if(o != null) {
			Employee emp = (Employee) o;
			if(this.getEmpno() == emp.getEmpno())
				return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return String.format("%5d     %-15s     %-15s     %5d     %-15s     %8.2f     %8.2f    %5d", this.empno, this.ename, this.job, this.mgr, this.hireDate, this.salary, this.comm, this.deptno);
	}
}
