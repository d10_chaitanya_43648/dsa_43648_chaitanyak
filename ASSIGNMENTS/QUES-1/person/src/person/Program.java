package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);

	public static List<Employee> getData() throws Exception{
		BufferedReader reader = new BufferedReader(new FileReader(new File("emp.csv")));
		String line = null;
		List<Employee> list = new ArrayList<Employee>();
		while( ( line = reader.readLine( ) ) != null ) { 
			String[] arr = line.split(",");
			if(arr[6].equalsIgnoreCase("NULL"))
				arr[6] = "0";
			if(arr[3].equalsIgnoreCase("NULL"))
				arr[3] = "0";
			if(arr[7].equalsIgnoreCase("NULL"))
				arr[7] = "0";
			Employee emp = new Employee(Integer.parseInt(arr[0]), arr[1], arr[2], Integer.parseInt(arr[3]), arr[4], Double.parseDouble(arr[5]), Double.parseDouble(arr[6]), Integer.parseInt(arr[7]));
			list.add(emp);
		}
		reader.close();
		return list;
	}
	
	private static void printData(List<Employee> list) {
		if(list != null) {
			for (Employee emp : list) {
				System.out.println(emp.toString());
			}
		}
	}
	
	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Load Data From File");
		System.out.println("2. Ascending order by name");
		System.out.println("3. Descending order by job");
		System.out.println("4. ascending order by deptno and descending order of sal (if deptno is same)");
		System.out.print("Enter Choice : ");
		return sc.nextInt();
	}
	
	public static void main(String[] args) throws Exception {
		List<Employee> list = null;
		Program.printData(list);
		int choice;
		while((choice = Program.menu()) != 0) {
			Comparator<Employee> c = null;
			switch(choice) {
			case 1:
				list = Program.getData();
				break;
			case 2:
				c = new SortByNameInAsc();
				list.sort(c);
				Program.printData(list);
				break;
			case 3:
				c = new SortByJobInDesc();
				list.sort(c);
				Program.printData(list);
				break;
			case 4:
				 Collections.sort(list, new EmployeeChainedComparator(
			                new SortByDeptno(),
			                new SortBySalary())
			        );
				 Program.printData(list);
				break;
			}
		}
	}


	

}
