package test;

import java.util.Scanner;

public class Program {
	static Scanner sc = new Scanner(System.in);
	
	private static void acceptposition(int[] pos) {
		System.out.print("Enter Position : ");
		pos[0] = sc.nextInt();
	}

	private static void acceptValue(int[] value) {
		System.out.print("Enter Data Element to Add : ");
		value[0] = sc.nextInt();
	}
	
	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Display Forward");
		System.out.println("2. Display Backword");
		System.out.println("3. Add Node at First");
		System.out.println("4. Add Node at Last");
		System.out.println("5. Add Node at Specific Position");
		System.out.println("6. Delete Node at First");
		System.out.println("7. Delete Node at Last");
		System.out.println("8. Delete Node at Specific Position");
		System.out.println("9. Delete All Nodes");
		System.out.println("Enter Choice : ");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		int choice;
		DoublyCircularLinkedList list = new DoublyCircularLinkedList();
		int[] value = new int[1];
		int[] pos = new int[1];
		while((choice = Program.menu()) != 0) {
			switch(choice) {
			case 1 ://Display All
				try {
					list.displayForward();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 2 ://Display All
				try {
					list.displayBackward();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 3 ://Add First
				Program.acceptValue(value);
				list.addFirst(value[0]);
				break;
			case 4 ://Add Last
				Program.acceptValue(value);
				list.addLast(value[0]);
				break;
			case 5 ://Add at pos
				Program.acceptValue(value);
				Program.acceptposition(pos);
				list.addAtPos(value[0], pos[0]);
				break;
			case 6 ://Del First
				try {
					list.delFirst();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 7 ://Del Last
				try {
					list.delLast();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 8 ://Del at pos
				try {
					Program.acceptposition(pos);
					list.delAtPos(pos[0]);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 9 ://Del All
				list.delAll();
				break;
			}
			System.out.println();
		}
	}

}
