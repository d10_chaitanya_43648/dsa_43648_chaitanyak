package test;

public class DoublyCircularLinkedList {
	// Node is static member class of DoublyCircularLinkedList
	static class Node{
		// Node class fields
		private int data;
		private Node prev;
		private Node next;
		// Node class methods
		public Node() {
			data = 0;
			prev = null;
			next = null;
		}
		public Node(int value) {
			data = value;
			prev = null;
			next = null;
		}
	}
	
	// DoublyCircularLinkedList class fields
	private Node head;
	
	// DoublyCircularLinkedList class methods
	public DoublyCircularLinkedList() {
		head = null;
	}
	
	//Check list is Empty or not
	public boolean isEmpty() {
		return head == null;
	}

	public void displayForward() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("Forward List : ");
			Node trav = head;
			do {
				System.out.println(trav.data);
				trav = trav.next;
			}while(trav != head);
			System.out.println();
		}
	}
	
	public void displayBackward() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("List : ");
			Node temp = null, trav = head;
			// traverse till last node
			while(trav.next != head)
				trav = trav.next;
			//store the last node reference into temp 
			temp = trav;
			// print all nodes in rev direction
			do {
				System.out.println(trav.data);
				trav = trav.prev;
			}while(trav != temp);
			System.out.println();
		}
	}

	public void addFirst(int value) {
		// create new node and init it
		Node newNode = new Node(value);
		// special 1: if list is empty
		if(isEmpty()) {
			head = newNode;
			newNode.next = newNode;
			newNode.prev = newNode;
		}
		// general: add node at the start
		else {
			// traverse till last node
			Node trav = head;
			while(trav.next != head)
				trav = trav.next;
			// new node next should point to head
			newNode.next = head;
			// new node prev should point to trav
			newNode.prev = trav;
			// head's prev to newNode
			head.prev = newNode;
			// head should point to new node
			head = newNode;
			//trav's next should point to first node
			trav.next = newNode;
		}
	}

	public void addLast(int value) {
		// create new node & init it
		Node newNode = new Node(value);
		// special 1: if list is empty, make new node as first node of list
		if(isEmpty()) {
			head = newNode;
			newNode.next = newNode;
			newNode.prev = newNode;
		}
		// general: add node at the end
		else {
			// traverse till last node
			Node trav = head;
			while(trav.next != head)
				trav = trav.next;
			// add new node after trav (trav.next)
			trav.next = newNode;
			// newnode's prev to last node (trav)
			newNode.prev = trav;
			// newnode's next to first node (head)
			newNode.next = head;
			//head's prev contain the reference of last node
			head.prev = newNode;
		}	
	}

	public void addAtPos(int value, int pos) {
		// special 1: if list is empty, add node at the start
		// special 2: if pos<=1, add node at the start
		if(isEmpty() || pos <= 1)
			addFirst(value);
		// general: add node at given pos
		else {
			// allocate & init new node
			Node newNode = new Node(value);
			// traverse till pos-1 (trav)
			Node temp = null, trav = head;
			for(int i=1; i < pos-1; i++) {
				// special 3: if pos > length of list, add node at the end.
				if(trav.next == head)
					break;
				trav = trav.next;
			}
			// take temp pointer to node after trav
			temp = trav.next;
			// newnode's next should point to trav's next i.e. temp
			newNode.next = temp;
			// newnode's prev to trav
			newNode.prev = trav;
			// trav's next should pointer to new node
			trav.next = newNode;
			// temp's prev to newNode
			if(temp != head) // special 3: if adding at end, next line is not required.
				temp.prev = newNode;
			//If newNode is last node then change head's prev to last node prev
			else
				head.prev = newNode;
		}
	}

	public void delFirst() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is empty.");
		// special 2: if list has single node, make head null
		if(head.next == head)
			head = null;
		// general: 
		else {
			// traverse till last node (trav)
			Node temp = head, trav = head;
			while(trav.next != head)
				trav = trav.next;
			// take head to the next (2nd) node
			head = head.next;
			head.prev = trav;
			// the last node (trav) next should be to new head
			trav.next = head;
			
			//temp is used to to make deleted node null
			temp.next = null;
			temp.prev = null;
		}		
	}
	
	public void delLast() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		// special 2: if list has single node, make head null.
		if(head.next == head)
			head = null;
		// general: delete the last node
		else {
			Node temp = null, trav = head;
			// traverse till last node (trav) and run temp behind it
			while(trav.next != head) {
				temp = trav;
				trav = trav.next;
			}
			// when last node (trav) is deleted, second last node (temp) next should be point to first node.
			temp.next = head;
			//first node's prev contain the reference of last node
			head.prev = temp;
			//trav's prev contain the reference previous node so make it null
			trav.prev = null;
			trav.next = null;
			// last node (trav) will be garbage collected.
		}
		
	}

	public void delAtPos(int pos) {
		// special 1: if pos = 1, delete first node
		if(pos == 1) {
			delFirst();
			return;
		}
		// special 2: if list is empty or pos < 1, then throw exception.
		if(isEmpty() || pos < 1)
			throw new RuntimeException("List is empty or Invalid position.");
		else {
			Node temp = null, trav = head;
			// traverse till pos (trav)
			for(int i = 1; i < pos; i++) {
				// special 3: if pos is beyond list length, then throw exception.
				if(trav.next == head)
					throw new RuntimeException("Invalid position.");
				temp = trav;
				trav = trav.next;
			}
			// temp next node to trav's next node
			temp.next = trav.next;
			// trav's next node's prev to trav's previous node
			if(trav.next != head) // special 3: while deleting last node, skip next line.
				trav.next.prev = trav.prev;
			// trav node will be garbage collected
			trav.next = null;
			trav.prev = null;
		}
	}

	public void delAll() {
		//break the circular chain so make any one in that chain is null
		head.next = null;
		head.prev = null;
		head = null;
		// all nodes will be garbage collected
	}
		
}
