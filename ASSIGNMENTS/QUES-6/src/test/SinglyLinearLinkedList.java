package test;

import java.util.function.Predicate;


public class SinglyLinearLinkedList {
	// Node is static member class of SinglyLinearLinkedList
	static class Node{
		// Node class fields
		private int data;
		private Node next;
		// Node class methods
		public Node() {
			data = 0;
			next = null;
		}
		public Node(int value) {
			data = value;
			next = null;
		}
	}
	
	// SinglyLinearLinkedList class fields
	private Node head;
	
	// SinglyLinearLinkedList class methods
	public SinglyLinearLinkedList() {
		head = null;
	}
	
	//Check list is Empty or not
	public boolean isEmpty() {
		return head == null;
	}

	public void displayAll() {
		//Check list is Empty or not
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			System.out.println("List : ");
			Node trav = head;
			while(trav != null) {
				System.out.println(trav.data);
				trav = trav.next;
			}
			System.out.println();
		}
	}

	public void addFirst(int value) {
		// create new node and init it
		Node newNode = new Node(value);
		if(isEmpty())
			head = newNode;
		else {
			// new node next should point to head
			newNode.next = head;
			// head should point to new node
			head = newNode;
		}
	}

	public void addLast(int value) {
		// create new node and init it
		Node newNode = new Node(value);
		// special 1: if list is empty, make new node as first node of list
		if(isEmpty())
			head = newNode;
		// general: add node at the end
		else {
			Node trav = head;
			// traverse till last node
			while(trav.next != null)
				trav = trav.next;
			// add new node after trav (trav.next)
			trav.next = newNode;
		}	
	}

	public void addAtPos(int value, int pos) {
		// special 1: if list is empty, add node at the start
		// special 2: if pos<=1, add node at the start
		if(pos <= 1 || isEmpty())
			addFirst(value);
		// general: add node at given pos
		else {
			// create new node and init it
			Node newNode = new Node(value);
			Node trav = head;
			// traverse till pos-1 (trav)
			for(int i = 1; i < pos - 1; i++) {
				// special 3: if pos > length of list, add node at the end.
				if(trav.next == null)
					break;
				trav = trav.next;
			}
			// newnode's next should point to trav's next
			newNode.next = trav.next;
			// trav's next should pointer to new nodes
			trav.next = newNode;
		}		
	}

	public void delFirst() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		else {
			// general: make head pointing to next node.
			head = head.next;
			// note: the old first node will be garbage collected.
		}		
	}
	
	public void delLast() {
		// special 1: if list is empty, throw exception
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		// special 2: if list has single node, make head null.
		if(head.next == null)
			head = null;
		// general: delete the last node
		else {
			Node temp = null, trav = head;
			// traverse till last node (trav) and run temp behind it
			while(trav.next != null) {
				temp = trav;
				trav = trav.next;
			}
			// when last node (trav) is deleted, second last node (temp) next should be null.
			temp.next = null;
			// last node (trav) will be garbage collected.
		}
		
	}

	public void delAtPos(int pos) {
		// special 1: if pos = 1, delete first node
		if(pos == 1) {
			delFirst();
			return;
		}
		// special 2: if list is empty or pos < 1, then throw exception.
		if(isEmpty() || pos < 1)
			throw new RuntimeException("List is empty or Invalid position.");
		else {
			// take temp pointer running behind trav.
			Node temp = null, trav = head;
			// traverse till pos (trav)
			for(int i = 1; i < pos; i++) {
				// special 3: if pos is beyond list length, then throw exception.
				if(trav.next == null)
					throw new RuntimeException("Invalid position.");
				temp = trav;
				trav = trav.next;
			}
			// trav is node to be deleted & temp is node before that
			temp.next = trav.next;
			// trav node will be garbage collected
		}
	}

	public void delAll() {
		head = null;
		// all nodes will be garbage collected
	}
	
	public int findMiddleNode() {
		if(isEmpty())
			throw new RuntimeException("List is Empty...");
		Node fast = head, slow = head;
		// For even numbers of nodes : fast != null 
		// For odd numbers of nodes : fast.next != null
		while(fast != null && fast.next != null && fast.next.next != null) {
			slow = slow.next;
			fast = fast.next.next;
		}
		return slow.data;
	}
	
	private Node reverse(Node h) {
		//Step 00 : If list has a single node mark it as a head and return it(it is a last node) 
		if(h.next == null) {
			head = h;
			return h;
		}
		//Step 01 : Reverse the rest of list (from next)
		Node t = reverse(h.next);
		//Step 02 : add current Node (h) after the last Node (t)
		t.next = h;
		//Step 03 : Make current Node next as a null, now current node become last node
		h.next = null;
		//Step 04 : Return Current Node i.e. Last Node
		return h;
	}
	
	//head is private field so called from this function
	public void reverse() {
		if(head != null)
			reverse(head);
		else
			throw new RuntimeException("List is Empty...");
	}

	public int countNumber(Predicate<Integer> pr) {
		int count = 0;
		Node trav = head;
		while(trav != null) {
			if(pr.test(trav.data))
				count++;
			trav = trav.next;
		}
		return count;
	}

	public void addDiff() {
		int cnt = 1;
		Node temp, trav = head;
		while(trav != null) {
			if(trav.next == null)
				return;
			temp = trav.next;
			int var = Math.abs(trav.data - temp.data);
			addAtPos(var, cnt);
			trav = temp.next;
			cnt = cnt + 3;
		}
	}
	
}
