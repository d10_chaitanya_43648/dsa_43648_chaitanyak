package test;

import java.util.Scanner;
import java.util.function.Predicate;

public class Program {
	static Scanner sc = new Scanner(System.in);
	
	private static void acceptposition(int[] pos) {
		System.out.print("Enter Position : ");
		pos[0] = sc.nextInt();
	}

	private static void acceptValue(int[] value) {
		System.out.print("Enter Data Element to Add : ");
		value[0] = sc.nextInt();
	}
	
	private static void acceptVal(int[] value) {
		System.out.print("Enter Data Element to Count : ");
		value[0] = sc.nextInt();
	}
	
	public static int menu() {
		System.out.println("0. Exit");
		System.out.println("1. Display All Elements");
		System.out.println("2. Add Node at First");
		System.out.println("3. Add Node at Last");
		System.out.println("4. Add Node at Specific Position");
		System.out.println("5. Delete Node at First");
		System.out.println("6. Delete Node at Last");
		System.out.println("7. Delete Node at Specific Position");
		System.out.println("8. Delete All Nodes");
		System.out.println("9. find middle node of list");
		System.out.println("10. Count number of integers matching given criteria");
		System.out.println("11. Reverse the linked list using recursion");
		System.out.println("12. Add difference of two consecutive nodes before them");
		System.out.println("Enter Choice : ");
		return sc.nextInt();
	}
	
	public static void main(String[] args) {
		int choice;
		SinglyLinearLinkedList list = new SinglyLinearLinkedList();
		int[] value = new int[1];
		int[] pos = new int[1];
		while((choice = Program.menu()) != 0) {
			switch(choice) {
			case 1 ://Display All
				try {
					list.displayAll();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 2 ://Add First
				Program.acceptValue(value);
				list.addFirst(value[0]);
				break;
			case 3 ://Add Last
				Program.acceptValue(value);
				list.addLast(value[0]);
				break;
			case 4 ://Add at pos
				Program.acceptValue(value);
				Program.acceptposition(pos);
				list.addAtPos(value[0], pos[0]);
				break;
			case 5 ://Del First
				try {
					list.delFirst();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 6 ://Del Last
				try {
					list.delLast();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 7 ://Del at pos
				try {
					Program.acceptposition(pos);
					list.delAtPos(pos[0]);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 8 ://Del All
				list.delAll();
				break;
			case 9 :
				try {
					int node = list.findMiddleNode();
					System.out.println("Middle Node : "+node);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 10 :
				try {
					Program.acceptVal(value);
					Predicate<Integer> pr = a -> (a == value[0]);  
					int cnt = list.countNumber(pr);
					System.out.println("Total Count of number "+value[0]+" is : "+cnt);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 11 :
				try {
					list.reverse();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			case 12 :
				try {
					list.addDiff();
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;
			}
			System.out.println();
		}
	}

}
