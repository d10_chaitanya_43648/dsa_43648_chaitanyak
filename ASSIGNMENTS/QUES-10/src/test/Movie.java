package test;


public class Movie {
	private int movieId;
	private String title, genres;
	public Movie() {
		this.movieId = 0;
		this.title = "";
		this.genres = "";
	}
	public Movie(int movieId, String title, String genres) {
		super();
		this.movieId = movieId;
		this.title = title;
		this.genres = genres;
	}
	public int getMovieId() {
		return movieId;
	}
	public void setMovieId(int movieId) {
		this.movieId = movieId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getGenres() {
		return genres;
	}
	public void setGenres(String genres) {
		this.genres = genres;
	}
	@Override
	public String toString() {
		return String.format("%-5d	%s			%s", this.movieId, this.title, this.genres);
	}
}
