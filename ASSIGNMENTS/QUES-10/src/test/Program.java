package test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.Scanner;

class HashTable {
	static class Pair {
		private int key; // key=roll
		private Movie value; // value=name

		public Pair() {
			key = 0;
			value = null;
		}

		public Pair(int key, Movie value) {
			this.key = key;
			this.value = value;
		}
	}

	private final int SLOTS = 100;
	private LinkedList<Pair> table[];

	public int hash(int key) {
		return key % SLOTS;
	}

	@SuppressWarnings("unchecked")
	public HashTable() {
		table = new LinkedList[SLOTS];
		for (int i = 0; i < SLOTS; i++)
			table[i] = new LinkedList<>();
	}

	public void put(int key, Movie value) {
		// find slot for given key using hash function
		int slot = hash(key);
		// access the bucket (linked list) in that slot
		LinkedList<Pair> bucket = table[slot];
		// find the element (key-value) in that bucket - linear search
		for (Pair pair : bucket) {
			// if key is duplicated, replace the value
			if (key == pair.key) {
				pair.value = value;
				return;
			}
		}
		// if key not found, create new pair and add into bucket
		Pair pair = new Pair(key, value);
		bucket.add(pair);
		// System.out.println("Added: " + pair);
	}

	public Movie get(int key) {
		// find slot for given key using hash function
		int slot = hash(key);
		// access the bucket (linked list) in that slot
		LinkedList<Pair> bucket = table[slot];
		// find the element (key-value) in that bucket - linear search
		for (Pair pair : bucket) {
			if (key == pair.key)
				return pair.value;
		}
		// if key not found, return null
		return null;
	}
}

public class Program {

	public static void main(String[] args) throws Exception {
		Scanner sc = new Scanner(System.in);
		HashTable ht = new HashTable();
		BufferedReader reader = new BufferedReader(new FileReader(new File("movies.csv")));
		String line = null;
		line = reader.readLine();
		while ((line = reader.readLine()) != null) {
			String[] arr = line.split("\\^");
			Movie m = new Movie(Integer.parseInt(arr[0]), arr[1], arr[2]);
			ht.put(Integer.parseInt(arr[0]), m);
		}
		
		System.out.print("Enter Movie to find (movieId) : ");
		int mvid = sc.nextInt();
		Movie m = ht.get(mvid);
		System.out.println("Movie found: " + m.toString());
		reader.close();
		sc.close();
	}
}
