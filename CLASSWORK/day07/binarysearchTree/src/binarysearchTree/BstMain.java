package binarysearchTree;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

class BinarySearch{
	 static class Node{
		private int data;
		private Node left,right;
		
		public Node() {
			data=0;
			left=right=null;
		}

		public Node(int val) {
			data=val;
			left=right=null;
		}
	}
	 
	 //tree fields
	 private Node root;
	 
	 public BinarySearch() {
		root=null;
	}
	 
	 //tree methods 
	 
	 public void add(int val) {
		 //create a node and initialize it
		 Node nn= new Node(val);
		 if(root==null)
			 root=nn;
		 else {
			 Node trav =root;
			 while(true){
			 if(val<trav.data) {
				 if(trav.left!=null)
					 trav=trav.left;
				 else //no child in the left
				 { 
					trav.left=nn;
					break;
				 }
			 }
			 
			 else {
				 if(trav.right!=null)
					 trav=trav.right;
				 else {
					 trav.right=nn;
					 break;
				 }
				
			 }
		 }
		 }
		
	 }
	 
	 public void preorder(Node trav) {
		 if(trav==null)
			 return;
		 System.out.print(trav.data+",");
		 preorder(trav.left);
		 preorder(trav.right);
	 }
	 
	 public void callpre() {
		 System.out.print("pre :");
		 preorder(root);
		 System.out.println();
	 }
	 
	 public void inorder(Node trav) {
		 if(trav==null)
			 return;
		 
		 inorder(trav.left);
		 System.out.print(trav.data+",");
		 inorder(trav.right);
	 }
	 
	 public void callin() {
		 System.out.print("in :");
		 inorder(root);
		 System.out.println();
	 }
	 
	 public void postorder(Node trav) {
		 if(trav==null)
			 return;
		 
		 postorder(trav.left);
		
		 postorder(trav.right);
		 System.out.print(trav.data+",");
	 }
	 
	 public void callpost() {
		 System.out.print("post :");
		 postorder(root);
		 System.out.println();
	 }
	 
	 public void deleteAll(Node trav) {
		if(trav==null)
			return;
		 deleteAll(trav.left);
		 deleteAll(trav.right);
		 trav.left=trav.right=null;
		 trav=null;
	 }
	 public void callDeleteAll() {
		
		 deleteAll(root);
		 root=null;
	 }
	 public int height(Node trav) {
		 if(trav==null)
			 return -1;
		int hl = height(trav.left);
		int hr = height(trav.right);
		int max = hl>hr?hl:hr;
		return max + 1;
	 }
	 public void callHeight() {
		 int height = height(root);
		 System.out.println(height);
	 }
	 
	 public void preorderNonrecursive() {
		 Stack<Node> s = new Stack<>();
		 Node trav=root;
		 while(trav!=null || !s.isEmpty()) {
		 while(trav!=null) {
			 System.out.print(trav.data + ", ");
			 s.push(trav.right);
			 trav=trav.left;
		 }
		if(!s.isEmpty()); 
		 trav=s.pop();
		 
	 }
	 }
	 
	public void inorderNonrecursive() {
		Stack<Node> s = new Stack<BinarySearch.Node>();
		Node trav = root;
		
		while(trav!=null || !s.isEmpty()) {
		while(trav!=null) {
			s.push(trav);
		trav=trav.left;
		}
		if(!s.isEmpty()) {
		trav= s.pop();
		System.out.println(trav.data);
		trav=trav.right;
		}
	}
}
	
	public Node bfs(int key) {
		Queue<Node> q = new LinkedList<>();
		q.offer(root);
		while(!q.isEmpty()) {
			Node trav = q.poll();
			if(key == trav.data)
				return trav;
			if(trav.left != null)
				q.offer(trav.left);
			if(trav.right != null)
				q.offer(trav.right);
		}
		return null;
	}
	
	public Node dfs(int key) {
		Stack<Node> s = new Stack<>();
		s.push(root);
		while(!s.isEmpty()) {
			Node trav = s.pop();
			if(key == trav.data)
				return trav;
			if(trav.right != null)
				s.push(trav.right);
			if(trav.left != null)
				s.push(trav.left);
		}
		return null;
	}
	
	public Node binarysearch(int key) {
		Node trav = root;
		while(trav!=null) {
		if(key == trav.data)
			return trav;
		if(key<trav.data)
			trav=trav.left;
		else
			trav=trav.right; 
	}
		return null;
	}
	
	public Node recursiveBinary(int key,Node trav) {
		Node temp = null;
		if(trav==null)
			return null;
		if(key==trav.data)
			return trav;
		if(key < trav.data) 
			temp = recursiveBinary( key, trav.left);
		else
			temp = recursiveBinary( key, trav.right);
		return temp;
	}
	public void recursiveBinary() {
		Node element = recursiveBinary(50, root);
		System.out.println(element.data);
	}
	public Node[] binarySearchwithparent(int key) {
		Node trav=root,parent=null;
		while(trav!=null) {
		if(key == trav.data)
			return new Node [] {trav,parent};
		parent=trav;
		if(key < trav.data)
			trav=trav.left;
		else
			trav=trav.right;
		}
		return new Node [] {null,null};
		
	}
	public void delete(int val) {
		Node trav, parent;
		// find the node to be deleted along with its parent
		Node[] arr = binarySearchwithparent(val);
		trav = arr[0];
		parent = arr[1];
		// if node is not found, throw the exception
		if(trav == null)
			throw new RuntimeException("Node not found.");
		// if node has left as well as right child
		if(trav.left != null && trav.right != null) {
			// find its successor with its parent
			parent = trav;
			Node succ = trav.right;
			while(succ.left != null) {
				parent = succ;
				succ = succ.left;
			}
			// overwrite data of node with successor data
			trav.data = succ.data;
			// mark successor node to be deleted
			trav = succ;
		}
		// if node has right child (or node doesn't have left child)
		if(trav.left == null) {
			if(trav == root)
				root = trav.right;
			else if(trav == parent.left)
				parent.left = trav.right;
			else
				parent.right = trav.right;
		}
		// if node has left child (or node doesn't have right child)
		else if(trav.right == null) {
			if(trav == root)
				root = trav.left;
			else if(trav == parent.left)
				parent.left = trav.left;
			else
				parent.right = trav.left;			
		}
	}
}
	

public class BstMain {
public static void main(String[] args) {
	BinarySearch t = new BinarySearch();
	t.add(50);
	t.add(30);
	t.add(10);
	t.add(90);
	t.add(100);
	t.add(40);
	t.add(70);
	t.add(80);
	t.add(60);
	t.add(20);
//	t.callDeleteAll();
//	t.callpre();
	t.callin();
	t.callpost();
	//t.callHeight();
	//t.preorderNonrecursive();
	t.delete(30);
	
	t.callin();
	
	
}

}
