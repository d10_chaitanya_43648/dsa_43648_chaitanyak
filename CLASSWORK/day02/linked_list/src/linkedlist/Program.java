package linkedlist;

import java.util.Arrays;
import java.util.Scanner;

import javax.management.RuntimeErrorException;

class SingleList{
	static class Node{
		private int data ;
		private Node next;
		public Node() {
			data=0;
			next=null;
			}
		public Node(int val) {
			data=val;
			next=null;
		}
		}
		private Node head;
		public SingleList() {
			head = null;
		}
	
		public void display() {
			System.out.println("List :");
			Node trav = head;
			while(trav!=null)
			{
				System.out.println(trav.data);
				trav=trav.next;
			}
			System.out.println("");
		}
		
		public void addLast(int val) {
			Node nn = new Node(val);
			if(head==null) {
				head = nn;
			}
			else {
			Node trav = head;
			while(trav.next != null) {
				trav= trav.next;
			}
			trav.next = nn;
			}
		}
		
		public void addFirst(int val) {
			Node nn = new Node(val);
			nn.next=head;
			head=nn;
		}
		
		public void addAtPos(int val , int pos) {
			Node nn = new Node(val);
			if(head == null || pos<=1) {
				addFirst(val);
			}
			Node trav = head;
			for (int i = 0; i < pos - 1; i++) {
				if(trav.next == null) {
					System.out.println("position is out of list");
					break;}
				trav = trav.next;
				}
			nn.next=trav.next;
			trav.next = nn;
		}
		public void deleteFirstElement() {
			if(head==null)
			{
				throw new RuntimeException("List is empty.");
			}
			else
			{
				head = head.next;
			}
		}
		
		public void deleteLast() {
			Node trav = head;
		}
		public void deleteAll() {
			head = null;
		}
		public void deleteAtPos(int pos) {
			if(pos==1) {
				deleteFirstElement();
			}
			if(head==null || pos < 1) {
				throw new RuntimeException("List is empty or Invalid position.");
			}
			Node temp = null;
			Node trav = head;
			for (int i = 1; i < pos; i++) {
				if(trav==null)
					throw new RuntimeException("Invalid position.");
				temp = trav;
				trav=trav.next;
			}
			temp.next=trav.next;
		}
		}

public class Program {
	
	public static void main(String[] args) {
		int choice,val,pos;
		Scanner sc = new Scanner(System.in);
		System.out.println("\n0. Exit\n1. Display\n2. Add First\n3.  Add Last\n4. Add At Pos\n5. Del First\n6.  Del Last\n7. Del At Pos\n8. Del All\nEnter choice: ");
		SingleList list = new SingleList();
		while((choice = sc.nextInt())!= 0 ) {
			
			switch(choice)
			{
			case 1:// Display
				 list.display();
				break;
			case 2:// Add First
				System.out.print("Enter new element: ");
				val = sc.nextInt();
				list.addFirst(val);
				break;
			case 3:// add last
				System.out.print("Enter new element: ");
				val = sc.nextInt();
				list.addLast(val);
				break;
			case 4://Add At Pos
				System.out.print("Enter new element: ");
				val = sc.nextInt();
				System.out.print("Enter pos of new element: ");
				pos = sc.nextInt();
				list.addAtPos(val, pos);
				break;
			case 5: //Del First
				list.deleteFirstElement();
				break;
			case 6:
				break;
			case 7:
				break;
			}
			
		}
		
		
	}
}



